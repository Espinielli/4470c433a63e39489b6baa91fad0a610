Use the range slider to change the degree of subdivision in this geodesic sphere.
The base shape, visible when subdivision is disabled, is either the [icosahedron](/mbostock/7782500)
or the [tetrahedron](https://en.wikipedia.org/wiki/Tetrahedron#Regular_tetrahedron).


Built with a modified version of the [d3.geodesic plugin](https://github.com/d3/d3-plugins/tree/master/geodesic).
Adapted to use D3v4.